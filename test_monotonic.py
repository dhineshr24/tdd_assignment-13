
from monotonic import Monotonic
import pytest
import os.path
from unittest.mock import MagicMock

class TestMonotonic:
    @classmethod
    def setup_class(cls):
        print("\nMonotonic increasing or decreasing")

    @classmethod
    def teardown_class(cls):
        print("\n* End *")

    def test_input_file(self):
        assert os.path.exists("input.txt") == 1

    @pytest.mark.parametrize("test_input,expected", [([1,2,3], True),([1,1,2,2,2,3], True),([4,3,2,1],True),([4,3,2,1,5],False)])
    def test_eval(self,test_input, expected):
        assert Monotonic.isMonotonic(test_input) == expected


    # def test_returns_correct_output(self):
    #     mock_file = MagicMock()
    #




