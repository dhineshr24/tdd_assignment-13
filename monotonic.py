

class Monotonic:
    def isMonotonic(A):

        return (all(A[i] <= A[i + 1] for i in range(len(A) - 1)) or
                all(A[i] >= A[i + 1] for i in range(len(A) - 1)))


    def readFromFile(self, file_name):
        infile = open(file_name, "r")
        line = infile.readline()
        return line



